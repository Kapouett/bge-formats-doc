# Big File

The Big File is the main data archive of the game.

The file is named `sally_clean.bf`, or `sally.bf` on some versions.

The index of a file in the metadata table is the same as its index in the offset table.

For indices, a value of `0xFFFFFFFF` is null.

Strings are encoded in Latin-1 (ISO-8859-1).

## Formats of files stored in the Big File

- `*.bin` are a custom format, detailed [here](Bin.md)
- `*.waa`, `*.wac`, `*.wad`, `*.wam` are standard `wav` files (MS-ADPCM)
- `*.bik` are Bink videos
- `*.mtx` ?
- `*.omd` ?
- `*.ofc` ?
- `*.oin` ?
- `*.ova` ?
- `*.wol` ?

## Header

| Size | Type | Description                                  |
|------|------|----------------------------------------------|
| 4    | str  | Magic bytes ("BIG\0")                        |
| 4    | ?    | ? (always 0x22, format version?)             |
| 4    | u32  | File count                                   |
| 4    | u32  | Directory count                              |
| 4    | u32  | ? (always 0x0)                               |
| 4    | ?    | ?                                            |
| 4    | ?    | ? (always 0xFFFFFFFF)                        |
| 4    | ?    | ?                                            |
| 4    | u32  | Offset table max length                      |
| 4    | ?    | ? (always 0x1)                               |
| 4    | u32  | Initial key (0x71003ff9)                     |
| 4    | u32  | File count (again?)                          |
| 4    | u32  | Directory count (again?)                     |
| 4    | u32  | Offset table offset (header size, always 68) |
| 4    | ?    | ? (always 0xFFFFFFFF)                        |
| 4    | ?    | ? (always 0x0)                               |
| 4    | u32  | Offset table max length - 1                  |

The file metadata table offset can be found after the offset table:  
`file_metadata_table_offset = offset_table_offset + offset_table_max_size * 8`.

The directory metadata table can be found after the file metadata table:  
`dir_metadata_table_offset = file_metadata_table_offset + offset_table_max_length * 0x54`

## Offset table entry

| Size            | Type | Description      |
|-----------------|------|------------------|
| 4               | u32  | File data offset |
| 4               | u32  | Resource key     |

## Directory metadata entry

The root directory has no parent (`0xFFFFFFFF`)

| Size | Type | Description                |
|------|------|----------------------------|
| 4    | u32  | First file index           |
| 4    | u32  | First subdir index         |
| 4    | u32  | Previous index             |
| 4    | u32  | Next index                 |
| 4    | u32  | Parent index               |
| 64   | str  | Dir name (null terminated) |

## File metadata entry

| Size | Type | Description                          |
|------|------|--------------------------------------|
| 4    | u32  | File size                            |
| 4    | u32  | Previous index                       |
| 4    | u32  | Next index                           |
| 4    | u32  | Directory index                      |
| 4    | u32  | Unix timestamp (last modification ?) |
| 64   | str  | File name (null terminated)          |

## File data entry

| Size      | Type | Description |
|-----------|------|-------------|
| 4         | u32  | File size   |
| File size | [u8] | Data        |
