# Beyond Good & Evil formats

This repository aims to gather information from various sources and additional research about the file formats used in the game [Beyond Good & Evil](https://wikipedia.org/wiki/Beyond_Good_%26_Evil_(video_game)) (2003).

## Table of contents

- [Big File](BigFile.md), the main archive file
- [`*.bin`](Bin.md)
- [`ff4*.bin`](AudioBank.md)
- [`fe*.bin`](AudioHeaders.md)
- [`fd*.bin`](TextFile.md)
- [`ff8*.bin`](TextureFile.md)
- [`ff0*.bin`](MapFile.md)

## Credits

- Most of the information about parsing the Big File comes from the README of this repository: https://github.com/panzi/bgebf
- **Zeli** provided a lot of various info
- Some info about parsing bin data comes from https://github.com/4g3v/JadeStudio
