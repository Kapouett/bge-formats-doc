# Map File ⚠️ **DRAFT** 🏗

These files are named `ff0*.bin` in the Big File.

| Size      | Type      | Description                      |
|-----------|-----------|----------------------------------|
| 4         | u32       | Keys size (in bytes)             |
| Keys size | [WOW key] | Keys                             |
| ?         | [WOW]     | WOWs                             |

## WOW key

| Size      | Type  | Description                      |
|-----------|-------|----------------------------------|
| 4         | u32   | Key                              |
| 4         | str   | Magic bytes (".wow")             |

## WOW

| Size      | Type   | Description                      |
|-----------|--------|----------------------------------|
| 4         | u32    | Size                             |
| 4         | str    | Magic bytes (".wow")             |
| 12        | [u8]   | Data                             |
| 60        | str    | Name                             |
| 64        | Matrix | Transform                        |
| 16        | ?      | Unknown                          |
...

## GAO

| Size      | Type   | Description                      |
|-----------|--------|----------------------------------|
| 4         | u32    | Size                             |
| 4         | str    | Magic bytes (".gao")             |
| 4         | u32    | Version                          |
| 4         | u32    | Unknown                          |
| Size - 12 | [u8]   | Data                             |
| ?         | ---    | Padding (just zeros)             |

## Matrix

4*4 row-major matrix.

| Size      | Type  | Description |
|-----------|-------|-------------|
| 4         | f32   | m00         |
| 4         | f32   | m01         |
| 4         | f32   | m02         |
| 4         | f32   | m03         |
| 4         | f32   | m10         |
| 4         | f32   | m11         |
| ...       | ...   | ...         |
| 4         | f32   | m33         |
