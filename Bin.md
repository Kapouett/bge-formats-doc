# Bin

The Big File contains many `*.bin` files. The first characters of their name indicates their content.

| Prefix | Content                                          |
|--------|--------------------------------------------------|
| `ff4*` | [Sound effects](AudioBank.md)                    |
| `fe*`  | [Sound headers](AudioHeaders.md)                 |
| `fd*`  | [Translated strings](TextFile.md)                |
| `ff8*` | [Textures](TextureFile.md)                       |
| `ff0*` | [Everything else: Gameobjects, meshes, materials…](MapFile.md) |

## Compressed files

All but `ff4*` contain a list of compressed blocks:

| Size            | Type | Description       |
|-----------------|------|-------------------|
| 4               | u32  | Decompressed size |
| 4               | u32  | Compressed size   |
| Compressed size | [u8] | Data              |

If the decompressed size is equal to the compressed size, this block is not compressed, and the data can be read as is, otherwise it should be decompressed with LZO.

If the decompressed size is 0, end of file was reached, any remaining data in the `.bin` should be padding.

The decompressed file is retreived by appending all the decompressed blocks.
