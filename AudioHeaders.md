# Audio Headers

These files are named `fe*.bin` in the Big File.

They contains multiple `wav` headers.

| Size | Type         | Description     |
|------|--------------|-----------------|
| 4    | u32          | Size (in bytes) |
| Size | [SoundEntry] | Sounds          |

## SoundEntry

| Size | Type | Description                          |
|------|------|--------------------------------------|
| 4    | u32  | Resource key                         |
| 46   | u32  | Header                               |
| 4    | u32  | Resource key again ?                 |
