# Audio Bank

These files are named `ff4*.bin` in the Big File.

They contains multiple `wav` files (MS-ADPCM).

| Size       | Type        | Description   |
|------------|-------------|---------------|
| 4          | u32         | Count         |
| 8 * Count  | [Sound key] | Keys          |
| 46 * Count | [[u8]]      | `wav` headers |
| ?          | [[u8]]      | `wav` data    |

Each header is 46 bytes long, and its 4 last bytes are the length of the sound's data block as a `u32`.

Individual audio files can be obtained by appending a `wav` header and its corresponding `wav` data (assuming its data is present in this `bin` file).

## Sound key

| Size | Type | Description                      |
|------|------|----------------------------------|
| 4    | u32  | Resource key                     |
| 4    | u32  | If non-zero, sound data is not contained in this file (resource key of another file containing the data?) |
