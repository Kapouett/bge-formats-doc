# Text file ⚠️ **DRAFT** 🏗

These files are named `fd*.bin` in the Big File.

| Size      | Type                 | Description              |
|-----------|----------------------|--------------------------|
| File size | [text groups]        | List of groups           |

## Text groups

| Size     | Type                 | Description              |
|----------|----------------------|--------------------------|
| 4        | u32                  | Size in bytes            |
| Size     | [group id entry]     | Group IDs (or keys)      |
| ?        | [group string refs]  | String IDs (or keys)     |
| ?        | [group strings]      | Strings                  |

## Group id entry

| Size | Type | Description              |
|------|------|--------------------------|
| 4    | u32  | Group id (resource key?) |
| 4    | str  | Magic (".txg")           |

## Group string refs

| Size | Type               | Description             |
|------|--------------------|-------------------------|
| 4    | u32                | Size in bytes           |
| Size | [string id entry]  | Text id (resource key?) |
| 4    | ?                  | ?                       |

## String id entry

| Size | Type | Description                  |
|------|------|------------------------------|
| 4    | u32  | Text id (resource key?)      |
| 4    | str  | Magic (".txi")               |
| 4    | u32  | String id (in strings array) |
| 4    | str  | Magic (".txs")               |

## Group strings

| Size         | Type  | Description             |
|--------------|-------|-------------------------|
| 4            | u32   | Breaks size (in bytes)  |
| Breaks size  | [u32] | Break positions         |
| 4            | u32   | Strings size (in bytes) |
| Strings size | str   | Concatenated strings    |

Using break positions, we can split the concatenated strings into the individual strings.
