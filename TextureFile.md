# Texture file ⚠️ **DRAFT** 🏗

These files are named `ff8*.bin` in the Big File.

| Size      | Type    | Description
|-----------|---------|-
| File size | [chunk] | Texture chunks

## Texture Type

| Type           | Value
|----------------|-
| TGA            | 0x1001
| BMP (?)        | 0x0002
| JPEG (?)       | 0x0003
| SpriteGen (?)  | 0x0004
| Procedural (?) | 0x0005
| 8bit paletted  | 0x4006
| 4bit paletted  | 0x5006
| Palette link   | 0x0007
| Animated (?)   | 0x0009

## Chunk

There are different types of chunks:
- Texture
- Palette
- PaletteLink
- FontDescription

Maybe the chunk type is stored in one of the unknown values ? For now I just guess it from the chunk's content while reading it.

### Texture

| Size            | Type | Description
|-----------------|------|-
| 4               | u32  | Chunk size
| 4               | u32  | Unknown (0xffffffff)
| 2               | ?    | Unknown
| 2               | u16  | Texture type
| 2               | u16  | Width
| 2               | u16  | Height
| 4               | ?    | Unknown
| 4               | u32  | FontDesc key
| 12              | [u8] | 0xcad0123400ff00ffc0dec0de
| Chunk size - 32 | [u8] | Texture data

Note that if chunk size == 64 && texture_type == Procedural, 4 additional bytes must be read.

### Palette

TODO

### Palette Link

| Size            | Type | Description
|-----------------|------|-
| 4               | u32  | Chunk size
| 4               | ?    | Unknown (0xffffffff)
| 2               | ?    | Unknown
| 2               | u16  | Texture type (0x0007)
| 4               | u32  | 0
| 4               | ?    | Unknown
| 4               | ?    | Unknown (0xFFFFFFFF)
| 12              | [u8] | 0xcad0123400ff00ffc0dec0de
| 4               | u32  | Texture key
| 4               | u32  | Palette key
| Chunk size - 40 |      | Padding

### Font Description

| Size            | Type            | Description
|-----------------|-----------------|-
| 4               | u32             | Chunk size
| 4               | ?               | Unknown (1)
| 8               | str             | "FONTDESC"
| Chunk size - 16 | [FontDescEntry] | Entries
| 4               | ?               | Unknown (0xFFFFFFFF)

#### FontDescEntry

| Size            | Type            | Description
|-----------------|-----------------|-
| 4               | u32             | Character
| 4               | f32             | x1
| 4               | f32             | y1
| 4               | f32             | x2
| 4               | f32             | y2
